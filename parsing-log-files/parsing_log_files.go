package parsinglogfiles

import "regexp"

func IsValidLine(text string) bool {
	reg, err := regexp.Compile(`^\[(TRC|DBG|INF|WRN|ERR|FTL)\]`)
	if err != nil {
		panic("Regex invalid")
	}
	return reg.MatchString(text)
}

func SplitLogLine(text string) []string {
	reg, err := regexp.Compile(`<[*~=-]*>`)
	if err != nil {
		panic("Regex invalid")
	}
	return reg.Split(text, -1)
}

func CountQuotedPasswords(lines []string) int {
	reg, err := regexp.Compile(`(?i)".*password.*"`)
	if err != nil {
		panic("Regex invalid")
	}
	passwordNumber := 0
	for _, line := range lines {
		if reg.MatchString(line) {
			passwordNumber++
		}
	}
	return passwordNumber
}

func RemoveEndOfLineText(text string) string {
	reg, err := regexp.Compile(`end-of-line\d*`)
	if err != nil {
		panic("Regex invalid")
	}
	return reg.ReplaceAllString(text, "")
}

func TagWithUserName(lines []string) []string {
	var tagged []string

	re := regexp.MustCompile(`User\s+([A-Za-z0-9]*)`)

	for _, line := range lines {
		submatches := re.FindStringSubmatch(line)
		if submatches != nil {
			tagged = append(tagged, "[USR] "+submatches[1]+" "+line)
		} else {
			tagged = append(tagged, line)
		}
	}
	return tagged
}
