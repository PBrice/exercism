package gross

// Units stores the Gross Store unit measurements.
func Units() map[string]int {
	return map[string]int{
		"quarter_of_a_dozen": 3,
		"half_of_a_dozen":    6,
		"dozen":              12,
		"small_gross":        120,
		"gross":              144,
		"great_gross":        1728,
	}
}

// NewBill creates a new bill.
func NewBill() map[string]int {
	return map[string]int{}
}

// AddItem adds an item to customer bill.
func AddItem(bill, units map[string]int, item, unit string) bool {
	_, unitExist := units[unit]
	if !unitExist {
		return false
	}
	//_, itemExists := bill[item]
	bill[item] += units[unit]
	/*if !itemExists {
		bill[item] = units[unit]
	} else {
		bill[item] += units[unit]
	}*/
	return true
}

// RemoveItem removes an item from customer bill.
func RemoveItem(bill, units map[string]int, item, unit string) bool {
	_, unitExist := units[unit]
	if !unitExist {
		return false
	}
	_, itemExist := bill[item]
	if !itemExist {
		return false
	}
	if units[unit] > bill[item] {
		return false
	}
	if units[unit] == bill[item] {
		delete(bill, item)
	} else {
		bill[item] -= units[unit]
	}
	return true
}

// GetItem returns the quantity of an item that the customer has in his/her bill.
func GetItem(bill map[string]int, item string) (int, bool) {
	value, itemExist := bill[item]
	return value, itemExist
}
