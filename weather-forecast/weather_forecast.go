/* Package weather : help to forecast weather events,
based on city location and actual weather condition.*/
package weather

// CurrentCondition of the weather.
var CurrentCondition string

// CurrentLocation we need to forecast.
var CurrentLocation string

// Forecast function take a city name and the current condition of that city, and return the forecast weather.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
