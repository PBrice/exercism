package lasagna

import "strings"

// TODO: define the 'PreparationTime()' function
func PreparationTime(layers []string, minutes int) (time int) {
	if minutes == 0 {
		return len(layers) * 2
	}
	return len(layers) * minutes
}

// TODO: define the 'Quantities()' function
func Quantities(layers []string) (grams int, sauce float64) {
	numberOfNoodles := strings.Count(strings.Join(layers, " "), "noodles")
	numberOfSauces := strings.Count(strings.Join(layers, " "), "sauce")
	return numberOfNoodles * 50, float64(numberOfSauces) * 0.2
}

// TODO: define the 'AddSecretIngredient()' function
func AddSecretIngredient(friendsList []string, myList []string) {
	myList[len(myList)-1] = friendsList[len(friendsList)-1]
}

// TODO: define the 'ScaleRecipe()' function
func ScaleRecipe(quantities []float64, portions int) []float64 {
	amounts := make([]float64, len(quantities))
	for index, amount := range quantities {
		amounts[index] = amount / 2 * float64(portions)
	}
	return amounts
}
