package thefarm

import (
	"errors"
	"fmt"
)

// See types.go for the types defined for this exercise.

// TODO: Define the SillyNephewError type here.
type SillyNephewError struct {
	message string
	details string
}

func (e *SillyNephewError) Error() string {
	return fmt.Sprintf("%s, %s", e.message, e.details)
}

// DivideFood computes the fodder amount per cow for the given cows.
func DivideFood(weightFodder WeightFodder, cows int) (float64, error) {
	fodder, err := weightFodder.FodderAmount()

	switch {
	case fodder > 0 && err == ErrScaleMalfunction:
		return 2 * fodder / float64(cows), nil
	case fodder < 0 && err == ErrScaleMalfunction:
		return 0, errors.New("negative fodder")
	case err != nil:
		return 0, err
	case fodder < 0:
		return 0, errors.New("negative fodder")
	case cows == 0:
		return 0, errors.New("division by zero")
	case cows < 0:
		return 0, &SillyNephewError{
			message: "silly nephew",
			details: fmt.Sprintf("there cannot be %v cows", cows)}
	}

	return fodder / float64(cows), nil
}
